package com.universeprojects.common.shared.callable;

public interface ReturningCallable4Args<R, A1, A2, A3, A4> extends ReturningCallable<R> {
    R call(A1 arg1, A2 arg2, A3 arg3, A4 arg4);
}
