package com.universeprojects.common.shared.callable;

public class KeyValuePair<K, V> {
    public final K key;
    public final V value;

    public KeyValuePair(K key, V value) {
        this.key = key;
        this.value = value;
    }
}
