package com.universeprojects.common.shared.callable;

public interface Callable3Args<A1, A2, A3> extends Callable {
    void call(A1 arg1, A2 arg2, A3 arg3);
}
