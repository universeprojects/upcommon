package com.universeprojects.common.shared.callable;

public interface Callable1Args<A1> extends Callable {
    void call(A1 arg1);
}
