package com.universeprojects.common.shared.profiler;

public interface ProfilerAutoCloseable extends AutoCloseable {
    @Override
    void close();
}
