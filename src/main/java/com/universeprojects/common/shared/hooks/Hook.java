package com.universeprojects.common.shared.hooks;

import com.universeprojects.common.shared.callable.Callable;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

public class Hook<C extends Callable> {
    private final int numArgs;

    Hook(int numArgs) {
        this.numArgs = numArgs;
    }

    private Set<C> callables;

    public int getNumArgs() {
        return numArgs;
    }

    Set<C> get(boolean create) {
        if (callables == null) {
            if (create) {
                synchronized (this) {
                    if (callables == null) {
                        callables = new LinkedHashSet<>();
                    }
                }
            } else {
                return Collections.emptySet();
            }
        }
        return callables;
    }

    Set<C> getAsCopy() {
        if(callables == null) {
            return Collections.emptySet();
        } else {
            return new LinkedHashSet<>(callables);
        }
    }
}
