package com.universeprojects.common.shared.hooks;

import com.universeprojects.common.shared.callable.Callable;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.common.shared.callable.Callable2Args;
import com.universeprojects.common.shared.callable.Callable3Args;

public final class Hooks {
    private Hooks() {
    }

    public static Hook<Callable0Args> createHook0Args() {
        return new Hook<>(0);
    }

    public static <C extends Callable1Args> Hook<C> createHook1Args() {
        return new Hook<>(1);
    }

    public static <C extends Callable2Args> Hook<C> createHook2Args() {
        return new Hook<>(2);
    }

    public static <C extends Callable3Args> Hook<C> createHook3Args() {
        return new Hook<>(3);
    }

    public static <C extends Callable> void subscribe(Hook<C> hook, C callable) {
        hook.get(true).add(callable);
    }

    public static <C extends Callable0Args> void subscribeAndCall(Hook<C> hook, C callable) {
        subscribe(hook, callable);
        callable.call();
    }

    public static <C extends Callable> void unsubscribe(Hook<C> hook, C callable) {
        hook.get(false).remove(callable);
    }

    public static void call(Hook<Callable0Args> hook) {
        for (Callable0Args callable : hook.getAsCopy()) {
            callable.call();
        }
    }

    public static <A1> void call(Hook<Callable1Args<A1>> hook, A1 arg1) {
        for (Callable1Args<A1> callable : hook.getAsCopy()) {
            callable.call(arg1);
        }
    }

    public static <A1, A2> void call(Hook<Callable2Args<A1, A2>> hook, A1 arg1, A2 arg2) {
        for (Callable2Args<A1, A2> callable : hook.getAsCopy()) {
            callable.call(arg1, arg2);
        }
    }

    public static <A1, A2, A3> void call(Hook<Callable3Args<A1, A2, A3>> hook, A1 arg1, A2 arg2, A3 arg3) {
        for (Callable3Args<A1, A2, A3> callable : hook.getAsCopy()) {
            callable.call(arg1, arg2, arg3);
        }
    }
}
