package com.universeprojects.common.shared.config;

import com.universeprojects.common.shared.util.Log;

import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("unused")
public abstract class ConfigurationBase {

    /**
     * Stores references to {@link ConfigProperty} instances that express the properties for this configuration
     */
    protected final List<ConfigProperty> configProperties = new LinkedList<>();

    private boolean anyManualWrite = false;

    protected ConfigurationBase() {

    }

    /**
     * Declares a string property with the specifies name.
     *
     * REQUIRED: won't pass validation if not set
     */
    public ConfigProperty<String> stringProperty(String name) {
        return stringProperty(name, null, true);
    }

    /**
     * Declares a string property with the specifies name.
     *
     * OPTIONAL: will use provided default value, but will fail if the default value is NULL
     */
    public ConfigProperty<String> stringProperty(String name, String defaultValue) {
        return stringProperty(name, defaultValue, true);
    }

    public ConfigProperty<String> stringProperty(String name, String defaultValue, boolean required) {
        ConfigProperty<String> property = new ConfigProperty<>(this, name, ConfigPropertyType.STRING, defaultValue, required);
        configProperties.add(property);
        return property;
    }


    /**
     * Declares a URL property with the specifies name.
     *
     * REQUIRED: won't pass validation if not set
     */
    public ConfigProperty<String> urlProperty(String name) {
        return urlProperty(name, null, true);
    }

    /**
     * Declares a URL property with the specifies name.
     *
     * OPTIONAL: will use provided default value, but will fail if the default value is NULL
     */
    public ConfigProperty<String> urlProperty(String name, String defaultValue) {
        return urlProperty(name, defaultValue, true);
    }

    public ConfigProperty<String> urlProperty(String name, String defaultValue, boolean required) {
        ConfigProperty<String> property = new ConfigProperty<>(this, name, ConfigPropertyType.URL, defaultValue, required);
        configProperties.add(property);
        return property;
    }

    /**
     * Declares an integer property with the specifies name.
     *
     * REQUIRED: won't pass validation if not set
     */
    public ConfigProperty<Integer> intProperty(String name) {
        return intProperty(name, null, true);
    }

    /**
     * Declares an integer property with the specifies name.
     *
     * OPTIONAL: will use provided default value
     */
    public ConfigProperty<Integer> intProperty(String name, Integer defaultValue) {
        return intProperty(name, defaultValue, true);
    }

    public ConfigProperty<Integer> intProperty(String name, Integer defaultValue, boolean required) {
        ConfigProperty<Integer> property = new ConfigProperty<>(this, name, ConfigPropertyType.INTEGER, defaultValue, required);
        configProperties.add(property);
        return property;
    }

    /**
     * Declares a boolean property with the specifies name.
     *
     * REQUIRED: won't pass validation if not set
     */
    public ConfigProperty<Boolean> booleanProperty(String name) {
        ConfigProperty<Boolean> property = new ConfigProperty<>(this, name, ConfigPropertyType.BOOLEAN, null, true);
        configProperties.add(property);
        return property;
    }

    /**
     * Declares a boolean property with the specifies name.
     *
     * OPTIONAL: will use provided default value
     */
    public ConfigProperty<Boolean> booleanProperty(String name, boolean defaultValue) {
        ConfigProperty<Boolean> property = new ConfigProperty<>(this, name, ConfigPropertyType.BOOLEAN, defaultValue, true);
        configProperties.add(property);
        return property;
    }

    /**
     * Declares a double property with the specifies name.
     *
     * REQUIRED: won't pass validation if not set
     */
    public ConfigProperty<Double> doubleProperty(String name) {
        return doubleProperty(name, null, true);
    }

    /**
     * Declares a double property with the specifies name.
     *
     * OPTIONAL: will use provided default value
     */
    public ConfigProperty<Double> doubleProperty(String name, Double defaultValue) {
        return doubleProperty(name, defaultValue, true);
    }

    public ConfigProperty<Double> doubleProperty(String name, Double defaultValue, boolean required) {
        ConfigProperty<Double> property = new ConfigProperty<>(this, name, ConfigPropertyType.DOUBLE, defaultValue, required);
        configProperties.add(property);
        return property;
    }

    /**
     * Declares a float property with the specifies name.
     * <p>
     * REQUIRED: won't pass validation if not set
     */
    public ConfigProperty<Float> floatProperty(String name) {
        return floatProperty(name, null, true);
    }

    /**
     * Declares a float property with the specifies name.
     * <p>
     * OPTIONAL: will use provided default value
     */
    public ConfigProperty<Float> floatProperty(String name, Float defaultValue) {
        return floatProperty(name, defaultValue, true);
    }

    public ConfigProperty<Float> floatProperty(String name, Float defaultValue, boolean required) {
        ConfigProperty<Float> property = new ConfigProperty<>(this, name, ConfigPropertyType.FLOAT, defaultValue, required);
        configProperties.add(property);
        return property;
    }

    /**
     * Sets a single property in the underlying properties store.
     * Useful for establishing default configurations
     *
     * @param propertyName The name of the property to set
     * @param propertyValue The value to assign to the property
     */
    protected abstract void writeProperty(String propertyName, String propertyValue);

    /**
     * This method is called to establish the actual configuration at runtime.
     * It's intended to be called after default configuration has been put in place.
     */
    @SuppressWarnings("unchecked")
    protected void loadConfiguration(boolean verbose) {
        if (verbose) {
            println("");
            println("Final configuration below (" + this.getClass().getName() + ")");
        }

        for (ConfigProperty property : configProperties) {
            final Object value = readProperty(property);
            property.setValueInternal(value);
            property.check();
            if (verbose)
                println(property.name + " = " + value);
        }

        anyManualWrite = false;

        if (verbose)
            println("");
    }

    /**
     * This method is called to establish the actual configuration at runtime.
     * It's intended to be called after default configuration has been put in place.
     */
    protected void loadConfiguration() {
        loadConfiguration(true);
    }

    private Object readProperty(ConfigProperty property) {
        switch (property.type) {
            case STRING:
                return readProperty(property.name, (String) property.defaultValue);
            case URL:
                return readUrlProperty(property.name, (String) property.defaultValue);
            case INTEGER:
                Integer intDefaultValue = null;
                if (property.defaultValue != null) {
                    intDefaultValue = ((Number) property.defaultValue).intValue();
                }
                return readIntegerProperty(property.name, intDefaultValue);
            case DOUBLE:
                Double dblDefaultValue = null;
                if (property.defaultValue != null) {
                    dblDefaultValue = ((Number) property.defaultValue).doubleValue();
                }
                return readDoubleProperty(property.name, dblDefaultValue);
            case FLOAT:
                Float fltDefaultValue = null;
                if (property.defaultValue != null) {
                    fltDefaultValue = ((Number) property.defaultValue).floatValue();
                }
                return readFloatProperty(property.name, fltDefaultValue);
            case BOOLEAN:
                return readBooleanProperty(property.name, (Boolean) property.defaultValue);
            default:
                throw new RuntimeException("Unhandled type: " + property.type);
        }
    }

    /**
     * Short-hand method to write to the console.
     * For dependency reasons, our logging framework can't be used here... must use the primitive system output.
     */
    protected void println(String line) {
        System.out.println("===  " + line);
    }


    /**
     * Retrieves a configuration property value in the following order:
     * <br/>
     * <br/>
     * 1. From a system property variable with the specified name
     * <br/>
     * 2. From an environment variable with the specified name
     * <br/>
     * 3. From the loaded properties, by the specified name
     * </br>
     *
     * @param propertyName The name of the property to retrieve
     * @return The String value of the property, NULL if not found
     */
    protected abstract String readProperty(String propertyName, String defaultValue);

    /**
     * Retrieves a URL property value, with the following adjustments:
     * <br/>
     * - the value is trimmed
     * <br/>
     * - if there's a trailing '/', it is removed
     * <br/>
     *
     * @param propertyName The name of the property to retrieve
     * @return The String value of the property, NULL if not found
     */
    protected String readUrlProperty(String propertyName, String defaultValue) {
        String value = readProperty(propertyName, null);
        if (value == null) {
            return defaultValue;
        }
        value = value.trim();
        if (value.endsWith("/")) {
            value = value.substring(0, value.length() - 1);
        }
        return value;
    }

    /**
     * Retrieves a boolean property value
     *
     * @param propertyName The name of the property to retrieve
     * @param defaultValue The default value to use, if the property is not set
     *
     * @return The boolean value of the property
     */
    @SuppressWarnings("SimplifiableIfStatement")
    protected boolean readBooleanProperty(String propertyName, boolean defaultValue) {
        String str = readProperty(propertyName, null);
        if ("true".equalsIgnoreCase(str))
            return true;
        else if ("false".equalsIgnoreCase(str))
            return false;
        else
            return defaultValue;
    }

    /**
     * Retrieves an integer property value
     *
     * @param propertyName The name of the property to retrieve
     * @param defaultValue The default value to use, if the property is not set
     *
     * @return The integer value of the property
     */
    protected Integer readIntegerProperty(String propertyName, Integer defaultValue) {
        String str = readProperty(propertyName, null);
        if(str == null) return defaultValue;
        try {
            return Integer.parseInt(str);
        }
        catch (NumberFormatException ex) {
            return defaultValue;
        }
    }


    /**
     * Retrieves an double property value
     *
     * @param propertyName The name of the property to retrieve
     * @param defaultValue The default value to use, if the property is not set
     *
     * @return The value of the property
     */
    protected Double readDoubleProperty(String propertyName, Double defaultValue) {
        String str = readProperty(propertyName, null);
        if(str == null) {
            return defaultValue;
        }
        try {
            return Double.parseDouble(str);
        } catch (NumberFormatException ex) {
            return defaultValue;
        }
    }

    /**
     * Retrieves an float property value
     *
     * @param propertyName The name of the property to retrieve
     * @param defaultValue The default value to use, if the property is not set
     * @return The value of the property
     */
    protected Float readFloatProperty(String propertyName, Float defaultValue) {
        String str = readProperty(propertyName, null);
        if (str == null) {
            return defaultValue;
        }
        try {
            return Float.parseFloat(str);
        } catch (NumberFormatException ex) {
            return defaultValue;
        }
    }

    public static void logLoadError(String filename, String propName, String msg, Throwable ex) {
        String err = "Error while reading file "+filename;
        if(propName != null)
            err += " property "+propName;
        if(msg != null)
            err += " "+msg;
        if(ex == null)
            Log.error(err);
        else
            Log.error(err,ex);
    }

    protected void onManualWrite(ConfigProperty<?> property) {
        anyManualWrite = true;
    }

    protected boolean hasAnyManualWrite() {
        return anyManualWrite;
    }

    public void resetAll() {
        for (ConfigProperty property : configProperties) {
            property.reset();
        }
    }
}
