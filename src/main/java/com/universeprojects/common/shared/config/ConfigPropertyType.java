package com.universeprojects.common.shared.config;

public enum ConfigPropertyType {
    STRING,
    URL,
    INTEGER,
    DOUBLE,
    FLOAT,
    BOOLEAN
}
