package com.universeprojects.common.shared.reflection;

import java.util.List;


public interface MetaDataOracle {
    <T,V> V getField(Class<T> instanceClass, T instance, Class<V> valueClass, String name);
    <T,V> void setField(Class<T> instanceClass, T instance, String name, V value);
    <T> T construct(Class<T> instanceClass);
    <T> List<String> getFieldNames(Class<T> instanceClass);
}
