package com.universeprojects.common.shared.reflection;

public class ReflectionException extends Exception {
    public ReflectionException() {
    }

    public ReflectionException(Throwable cause) {
        super(cause);
    }
}
