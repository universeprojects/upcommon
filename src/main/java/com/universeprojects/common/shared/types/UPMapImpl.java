package com.universeprojects.common.shared.types;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

@SuppressWarnings({"serial", "GwtInconsistentSerializableClass"})
public class UPMapImpl<K, V> extends LinkedHashMap<K, V> implements UPMap<K, V> {

    protected final Class<K> keyClass;
    protected final Class<V> valueClass;

    public UPMapImpl(Class<K> keyClass, Class<V> valueClass) {
        this.keyClass = keyClass;
        this.valueClass = valueClass;
    }

    public UPMapImpl(Map<K, V> map, Class<K> keyClass, Class<V> valueClass) {
        super(map);
        this.keyClass = keyClass;
        this.valueClass = valueClass;
    }

    public UPMapImpl(UPMapImpl<K, V> mapToCopy) {
        this(mapToCopy, mapToCopy.getKeyClass(), mapToCopy.getValueClass());
    }

    @Override
    public Class<K> getKeyClass() {
        return keyClass;
    }

    @Override
    public Class<V> getValueClass() {
        return valueClass;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result
            + ((keyClass == null) ? 0 : keyClass.hashCode());
        result = prime * result
            + ((valueClass == null) ? 0 : valueClass.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        UPMapImpl<?, ?> other = (UPMapImpl<?, ?>) obj;
        return keyClass == other.keyClass && valueClass == other.valueClass;
    }

    @Override
    public Set<java.util.Map.Entry<K, V>> modifiableEntrySet() {
        return entrySet();
    }

    @Override
    public V put(K key, V value) {
        if(key == null) {
            throw new IllegalStateException("UPMap Key cannot be null");
        }
        return super.put(key, value);
    }
}
