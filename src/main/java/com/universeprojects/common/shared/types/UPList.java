package com.universeprojects.common.shared.types;

import java.util.List;

public interface UPList<E> extends List<E> {
    Class<E> getElementClass();
}
