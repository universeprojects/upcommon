package com.universeprojects.common.shared.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.util.ArrayList;

@Target({ElementType.FIELD, ElementType.PARAMETER})
public @interface SerializableList {
    Class elementClass();

    Class listClass() default ArrayList.class;
}
