package com.universeprojects.common.shared.util;


public enum Environment {
    SERVER(false, false), CLIENT(true, false), TEST(false, true);
    public static Environment current = Environment.CLIENT;
    private final boolean isClient;
    private final boolean isTest;

    Environment(boolean isClient, boolean isTest) {
        this.isClient = isClient;
        this.isTest = isTest;
    }

    public boolean isClient() {
        return isClient;
    }

    public boolean isServer() {
        return !isClient;
    }

    public boolean isTest() {
        return isTest;
    }
}
