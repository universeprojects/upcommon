package com.universeprojects.common.shared.util;

/**
 * This rather trivial class is helpful when we would like to keep a counter of something,
 * but the declaration is forced to be final. For that reason, it can't be an int / Integer, which are immutable.
 *
 * Example use case:
 * Object references accessed from within anonymous classes must be declared as final in Java (limitation).
 *
 * It is also useful when we'd like to constrain a value's modification to "increment by 1" only
 *
 */
public class SimpleCounter {

    private final static int DEFAULT_START_VALUE = 0;

    private int value;

    public SimpleCounter() {
        this(DEFAULT_START_VALUE);
    }

    public SimpleCounter(int startValue) {
        this.value = startValue;
    }

    public void increment() {
        value++;
    }

    public int value() {
        return value;
    }

    public void reset() {
        reset(DEFAULT_START_VALUE);
    }

    public void reset(int startValue) {
        this.value = startValue;
    }

    @Override
    public String toString() {
        return ""+value;
    }
}
