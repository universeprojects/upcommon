package com.universeprojects.common.shared.util;

public class TimeUtil {

    public static final Long MS_PER_SECOND = 1000L;

    public static final Long MS_PER_MINUTE = 60 * MS_PER_SECOND;

    public static final Long MS_PER_HOUR = 60 * MS_PER_MINUTE;

    public static final Long MS_PER_DAY = 24 * MS_PER_HOUR;


    /**
     * Converts a length of time in milliseconds to a human-readable representation, according to the following rules:
     *
     *  - The input can't be negative
     *  - For input less than 1 second the output is "N milliseconds"
     *  - For input less than 1 minute the output is "N seconds"
     *  - For input less than 1 hour the output is in "MM:SS" format
     *  - For any other input the output is in "H:MM:SS" format
     */
    public static String millisToReadableStr(long millis) {
        if (millis < 0) {
            throw new IllegalArgumentException("Millisecond value can't be negative");
        }

        if (millis < 1000) {
            return millis + " milliseconds";
        }

        int seconds = (int) (millis / 1000);
        int minutes = seconds / 60;

        int ss = seconds % 60;
        int mm = minutes % 60;
        int hh = minutes / 60;

        if (hh > 0) {
            return hh + ":" + formatInt(mm) + ":" + formatInt(ss);
        }
        else if (mm > 0) {
            return formatInt(mm) + ":" + formatInt(ss);
        }
        else {
            return seconds + " seconds";
        }
    }

    private static String formatInt(int num) {
        String str = String.valueOf(num);
        if(num < 10) {
            str = "0"+str;
        }
        return str;
    }

}
