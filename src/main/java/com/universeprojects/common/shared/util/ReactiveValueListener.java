package com.universeprojects.common.shared.util;

public interface ReactiveValueListener<T> {
    void onChange(T oldValue, T newValue);
}
