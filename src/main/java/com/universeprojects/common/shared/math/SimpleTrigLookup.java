package com.universeprojects.common.shared.math;

public class SimpleTrigLookup {

	private static final double radFull = UPMath.PI * 2.0d;
	private static final double degFull = 360.0d;

	private static final double degToRad = UPMath.PI / 180.0d;
	private static final double radToDeg = 180.0d / UPMath.PI;

	private static final double precision = 0.25; // of a degree
	private static final int numEntries = (int)(360d / precision);

	private static final double[] sin = new double[numEntries];
	private static final double[] cos = new double[numEntries];

	private static final double radToIndex = numEntries / radFull;
	private static final double degToIndex = numEntries / degFull;

	static {
		for (int i = 0; i < numEntries; i++) {
			sin[i] = UPMath.sin((i + 0.5d) / radToIndex);
			cos[i] = UPMath.cos((i + 0.5d) / radToIndex);
		}

		// Four cardinal directions need to be precise
		int[] cardinalDir = {0, 90, 180, 270};
		for (int i : cardinalDir) {
			sin[(int) (i * degToIndex)] = UPMath.sin(i * degToRad);
			cos[(int) (i * degToIndex)] = UPMath.cos(i * degToRad);
		}
	}

	private static int radToIndex(double rad) {
		rad = rad % radFull;
		if (rad < 0) {
			rad += radFull;
		}
		int index = (int) (rad * radToIndex);

		return index;
	}

	private static int degToIndex(double deg) {
		deg = deg % degFull;
		if (deg < 0) {
			deg += degFull;
		}
		int index = (int) (deg * degToIndex);

		return index;
	}

	public static final double sin(double rad) {
		return sin[radToIndex(rad)];
	}

	public static final double cos(double rad) {
		return cos[radToIndex(rad)];
	}

	public static final double tan(double rad) {
		return sin(rad) / cos(rad);
	}

	public static final double sinDeg(double deg) {
		return sin[degToIndex(deg)];
	}

	public static final double cosDeg(double deg) {
		return cos[degToIndex(deg)];
	}

	public static final double tanDeg(double deg) {
		return sinDeg(deg) / cosDeg(deg);
	}

}
