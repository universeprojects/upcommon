package com.universeprojects.common.shared.math;

public class CoordinateEncoder {
    public static final CoordinateEncoder encoderSigned = new CoordinateEncoder();
    public static final CoordinateEncoder encoderUnsigned = new CoordinateEncoder(0);

    public static final int MAX_BITS = 30;
    public static final int MAX_VALUE = (1 << MAX_BITS);
    public static final CoordinateEncoder MAX_ENCODER = new CoordinateEncoder(1 << (MAX_BITS - 1));
    private final int offset;

    /**
     * This version of the constructor assumes the offset should be 536870912 which is half of a 30 bit number.
     * This allows x/y values that have an equal range of positive and negative numbers.
     */
    public CoordinateEncoder(){
        this(536870912);
    }

    public CoordinateEncoder(int offset) {
        this.offset = offset;
    }

    public int getOffset() {
        return offset;
    }

    public long encodeLocHash(int x, int y) {
        // input is limited to 30 bits + offset for map wrapping
        // 0-clientRange-maxXXXSize -> 3FFFFFFF+clientRange+maxXXXSize
        // Ex Char FFFFFFF1 to 4000000E

        // add a big enough offset to ensure we have a positive number
        // needs to remain a 31bit number though
        // 3D9 to 400003F6
        long lx = (long) x + offset;
        long ly = (long) y + offset;

        if (lx < 0 || ly < 0 || lx > MAX_VALUE || ly > MAX_VALUE) {
            throw new IllegalArgumentException("The encoder only works with up to 30 bit values including offset. Provided: x=" + x + ", y=" + y + ", offset=" + offset);
        }

        //interlace bits
        long locHash = 0;
        for (int i = 0; i <= MAX_BITS; i++) {
            locHash |= ((lx & (1 << i)) << (i + 1)) | ((ly & (1 << i)) << i);
        }
        return locHash;
    }

    public int[] decodeLocHash(long locHash) {
        long lx = 0;
        long ly = 0;
        for (int i = 0; i <= MAX_BITS; i++) {
            lx |= (locHash & (1L << (2 * i + 1))) >> (i + 1);
            ly |= (locHash & (1L << (2 * i))) >> (i);
        }
        int x = (int) (lx - offset);
        int y = (int) (ly - offset);
        return new int[]{x, y};
    }
}
