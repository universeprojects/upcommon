package com.universeprojects.common.shared.math;


import java.util.Random;

@SuppressWarnings("unused")
public class MathTest {

    private MathTest() {
    }

    private static final int STARTVAL1_INT = 723253456;
    private static final int STARTVAL2_INT = 8456;
    private static final long STARTVAL1_LONG = 49071029458l;
    private static final long STARTVAL2_LONG = 355129458l;
    private static final double STARTVAL1_FLOAT = 2.484E4f;
    private static final double STARTVAL2_FLOAT = 0.112454f;
    private static final double STARTVAL1_DOUBLE = 7.123124563577E11;
    private static final double STARTVAL2_DOUBLE = 4.56444123577E-5;
    private static final double STARTVAL_SQRT = 111.97746;
    private static final double STARTVAL1_TRIG = 31.345346;
    private static final double STARTVAL2_TRIG = 0.9879345;
    private static final int STARTVAL_RND = 768068074;
    private static final int NUMTESTS = 20;

    public static final String DELIMITER = "|";
    public static final String DELIMITER_ESCAPED = "\\|";
    public static final String TEST_DELIMITER = DELIMITER + DELIMITER;
    public static final String TEST_DELIMITER_ESCAPED = DELIMITER_ESCAPED + DELIMITER_ESCAPED;

    private static String result = null;

    public static String run() {
        if (result != null) return result;
        StringBuilder sb = new StringBuilder(TEST_DELIMITER);
        //important
        testInt(sb);
        testDouble(sb);
        testTrig(sb);
//        testMathRest(sb);
        //testOurRnd(sb);

        testPow(sb);
//
//        //rest
//        testLong(sb);
//        testFloat(sb);
        testJavaRnd(sb);
//
//        //rnd
//        testRndFloats(sb);
//        testRndDoubles(sb);
//        testRndSqrt(sb);
        testRndTrig(sb);

        //lookup
//		testTrigLookup(sb);

        result = sb.toString();
        return result;
    }


    public static String withNL() {
        return withNL(run());
    }

    public static String format(double val) {
        return Long.toString(Double.doubleToLongBits(val), 36);
        //return NumberFormat.getDefaultInstance().format(val);
    }

    public static String withNL(String result) {
        return result.replaceAll(TEST_DELIMITER_ESCAPED, "\n");
    }

    public static String diffWithNL(String name1, String res1, String name2, String res2) {
        String[] split1 = res1.split(TEST_DELIMITER_ESCAPED);
        String[] split2 = res2.split(TEST_DELIMITER_ESCAPED);
        if (split1.length != split2.length) {
            return name1 + " has a different number of elements than " + name2 + ": " + split1.length + " vs " + split2.length + "\n" + name1 + ": " + res1 + "\n" + name2 + ": " + res2;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < split1.length; i++) {
            if (split1[i].isEmpty()) continue;
            if (split1[i].equals(split2[i])) {
                sb.append(split1[i].split(DELIMITER_ESCAPED)[0]).append(" EQUAL").append("\n");
            } else {
                sb.append(split1[i].split(DELIMITER_ESCAPED)[0]).append(" DIFFERENT").append("\n");
                sb.append(name1).append(split1[i]).append("\n");
                sb.append(name2).append(split2[i]).append("\n");
            }
        }
        return sb.toString();
    }

    private static void testInt(StringBuilder sb) {
        sb.append("int").append(DELIMITER);
        sb.append(Integer.MAX_VALUE).append(DELIMITER);
        sb.append(Integer.MIN_VALUE).append(DELIMITER);
        sb.append(Integer.MAX_VALUE / 2).append(DELIMITER);
        sb.append(STARTVAL1_INT).append(DELIMITER);
        sb.append(STARTVAL2_INT).append(DELIMITER);
        sb.append(STARTVAL1_INT + STARTVAL2_INT).append(DELIMITER);
        //noinspection NumericOverflow
        sb.append(STARTVAL1_INT * STARTVAL2_INT).append(DELIMITER);
        sb.append(STARTVAL1_INT / STARTVAL2_INT).append(DELIMITER);
        sb.append(STARTVAL1_INT % STARTVAL2_INT).append(DELIMITER);
        sb.append(DELIMITER);
    }

    private static void testDouble(StringBuilder sb) {
        sb.append("double").append(DELIMITER);
        //sb.append(format(Double.MAX_VALUE)).append(DELIMITER);
        //sb.append(format(Double.MIN_VALUE)).append(DELIMITER);
        //sb.append(format(Double.MAX_VALUE/2.)).append(DELIMITER);
        //sb.append(format(Double.MIN_VALUE*2.)).append(DELIMITER);
        //sb.append(Double.toHexString(1.633123935319537E16)).append(DELIMITER);
        //sb.append(Double.doubleToLongBits(1.633123935319537E16)).append(DELIMITER);
        //sb.append(Double.toString(1.633123935319537E16)).append(DELIMITER);
        //sb.append(format(2*1.633123935319537E16)).append(DELIMITER);
        sb.append(format(STARTVAL1_DOUBLE)).append(DELIMITER);
        sb.append(format(STARTVAL2_DOUBLE)).append(DELIMITER);
        sb.append(format(STARTVAL1_DOUBLE + STARTVAL2_DOUBLE)).append(DELIMITER);
        sb.append(format(STARTVAL1_DOUBLE * STARTVAL2_DOUBLE)).append(DELIMITER);
        sb.append(format(STARTVAL1_DOUBLE / STARTVAL2_DOUBLE)).append(DELIMITER);
        sb.append(format(STARTVAL1_DOUBLE % STARTVAL1_INT)).append(DELIMITER);
        double pow = STARTVAL1_DOUBLE;
        for (int i = 0; i < NUMTESTS; i++)
            pow *= STARTVAL1_DOUBLE;
        sb.append(format(pow)).append(DELIMITER);
        double div = STARTVAL1_DOUBLE;
        for (int i = 0; i < NUMTESTS; i++)
            div /= STARTVAL2_DOUBLE;
        sb.append(format(div)).append(DELIMITER);
        sb.append(DELIMITER);
    }

    private static void testLong(StringBuilder sb) {
        sb.append("long").append(DELIMITER);
        sb.append(Long.MAX_VALUE).append(DELIMITER);
        sb.append(Long.MIN_VALUE).append(DELIMITER);
        sb.append(STARTVAL1_LONG).append(DELIMITER);
        sb.append(STARTVAL2_LONG).append(DELIMITER);
        sb.append(STARTVAL1_LONG + STARTVAL2_LONG).append(DELIMITER);
        //noinspection NumericOverflow
        sb.append(STARTVAL1_LONG * STARTVAL2_LONG).append(DELIMITER);
        sb.append(STARTVAL1_LONG / STARTVAL2_LONG).append(DELIMITER);
        sb.append(DELIMITER);
    }

    private static void testFloat(StringBuilder sb) {
        sb.append("float").append(DELIMITER);
        sb.append(format(Float.MAX_VALUE)).append(DELIMITER);
        sb.append(format(Float.MIN_VALUE)).append(DELIMITER);
        sb.append(format(Float.MAX_VALUE / 2f)).append(DELIMITER);
        sb.append(format(Float.MIN_VALUE * 2f)).append(DELIMITER);
        sb.append(format(STARTVAL1_FLOAT)).append(DELIMITER);
        sb.append(format(STARTVAL2_FLOAT)).append(DELIMITER);
        sb.append(format(STARTVAL1_FLOAT + STARTVAL2_FLOAT)).append(DELIMITER);
        sb.append(format(STARTVAL1_FLOAT * STARTVAL2_FLOAT)).append(DELIMITER);
        sb.append(format(STARTVAL1_FLOAT / STARTVAL2_FLOAT)).append(DELIMITER);
        double pow = STARTVAL1_FLOAT;
        for (int i = 0; i < NUMTESTS; i++)
            pow *= STARTVAL1_FLOAT;
        sb.append(format(pow)).append(DELIMITER);
        double div = STARTVAL1_FLOAT;
        for (int i = 0; i < NUMTESTS; i++)
            div /= STARTVAL2_FLOAT;
        sb.append(DELIMITER);
    }

    private static void testMathRest(StringBuilder sb) {
        sb.append("sqrt").append(DELIMITER);
        sb.append(format(STARTVAL_SQRT)).append(DELIMITER);
        sb.append(format(UPMath.sqrt(STARTVAL_SQRT))).append(DELIMITER);
        sb.append(format(UPMath.sqrt(STARTVAL_SQRT * STARTVAL_SQRT))).append(DELIMITER);
        sb.append(DELIMITER);

        sb.append("constants").append(DELIMITER);
        sb.append(format(UPMath.PI)).append(DELIMITER);
        sb.append(format(UPMath.E)).append(DELIMITER);
        sb.append(DELIMITER);
    }

    private static void testTrig(StringBuilder sb) {
        sb.append("trig").append(DELIMITER);
        sb.append(format(STARTVAL1_TRIG)).append(DELIMITER);
        sb.append(format(STARTVAL2_TRIG)).append(DELIMITER);
        sb.append(format(TrigFacade.sin(STARTVAL1_TRIG))).append(DELIMITER);
        sb.append(format(TrigFacade.cos(STARTVAL1_TRIG))).append(DELIMITER);
        sb.append(format(TrigFacade.tan(STARTVAL1_TRIG))).append(DELIMITER);
        sb.append(format(TrigFacade.sin(STARTVAL2_TRIG))).append(DELIMITER);
        sb.append(format(TrigFacade.cos(STARTVAL2_TRIG))).append(DELIMITER);
        sb.append(format(TrigFacade.tan(STARTVAL2_TRIG))).append(DELIMITER);
        sb.append(format(TrigFacade.asin(STARTVAL2_TRIG))).append(DELIMITER);
        sb.append(format(TrigFacade.acos(STARTVAL2_TRIG))).append(DELIMITER);
        sb.append(format(TrigFacade.atan(STARTVAL2_TRIG))).append(DELIMITER);
        sb.append(format(TrigFacade.sin(TrigFacade.asin(STARTVAL2_TRIG)))).append(DELIMITER);
        sb.append(format(TrigFacade.cos(TrigFacade.acos(STARTVAL2_TRIG)))).append(DELIMITER);
        sb.append(format(TrigFacade.tan(TrigFacade.atan(STARTVAL2_TRIG)))).append(DELIMITER);
        sb.append(format(TrigFacade.asin(TrigFacade.sin(STARTVAL1_TRIG)))).append(DELIMITER);
        sb.append(format(TrigFacade.acos(TrigFacade.cos(STARTVAL1_TRIG)))).append(DELIMITER);
        sb.append(format(TrigFacade.atan(TrigFacade.tan(STARTVAL1_TRIG)))).append(DELIMITER);
        sb.append(format(TrigFacade.atan2(STARTVAL1_TRIG, STARTVAL2_TRIG))).append(DELIMITER);
        sb.append(format(TrigFacade.atan2(STARTVAL2_TRIG, STARTVAL1_TRIG))).append(DELIMITER);
        sb.append(format(TrigFacade.atan2(STARTVAL1_TRIG, TrigFacade.tan(STARTVAL1_TRIG)))).append(DELIMITER);
        sb.append(format(TrigFacade.atan2(STARTVAL2_TRIG, TrigFacade.atan(STARTVAL2_TRIG)))).append(DELIMITER);
        sb.append(DELIMITER);
    }

    /**
     * private static void testOurRnd(StringBuilder sb) {
     * sb.append("ourRnd").append(DELIMITER);
     * sb.append(STARTVAL_RND).append(DELIMITER);
     * <p/>
     * }
     **/

    private static void testJavaRnd(StringBuilder sb) {
        sb.append("javaRnd").append(DELIMITER);
        sb.append(STARTVAL_RND).append(DELIMITER);
        java.util.Random rnd = new java.util.Random(STARTVAL_RND);
        sb.append(rnd.nextInt()).append(DELIMITER);
        sb.append(rnd.nextInt(100)).append(DELIMITER);
        sb.append(format(rnd.nextDouble())).append(DELIMITER);
        sb.append(DELIMITER);
    }

    private static void testRndFloats(StringBuilder sb) {
        sb.append("rndFlt").append(DELIMITER);
        sb.append(STARTVAL_RND).append(DELIMITER);
        java.util.Random rnd = new java.util.Random(STARTVAL_RND);
        for (int i = 0; i < NUMTESTS; i++) {
            sb.append(format(rnd.nextFloat())).append(DELIMITER);
        }
        sb.append(DELIMITER);
    }

    private static void testRndDoubles(StringBuilder sb) {
        sb.append("rndDbl").append(DELIMITER);
        sb.append(STARTVAL_RND).append(DELIMITER);
        java.util.Random rnd = new java.util.Random(STARTVAL_RND);
        for (int i = 0; i < NUMTESTS; i++) {
            sb.append(format(rnd.nextDouble())).append(DELIMITER);
        }
        sb.append(DELIMITER);
    }

    private static void testRndSqrt(StringBuilder sb) {
        sb.append("rndSqrt").append(DELIMITER);
        sb.append(STARTVAL_RND).append(DELIMITER);
        java.util.Random rnd = new java.util.Random(STARTVAL_RND);
        for (int i = 0; i < NUMTESTS; i++) {
            sb.append(format(UPMath.sqrt(rnd.nextDouble()))).append(DELIMITER);
        }
        sb.append(DELIMITER);
    }

    private static void testRndTrig(StringBuilder sb) {
        sb.append("rndTrigSin").append(DELIMITER);
        sb.append(STARTVAL_RND).append(DELIMITER);
        java.util.Random rnd = new java.util.Random(STARTVAL_RND);
        for (int i = 0; i < NUMTESTS; i++) {
            sb.append(format(TrigFacade.sin(rnd.nextDouble()))).append(DELIMITER);
        }
        sb.append(DELIMITER);

        sb.append("rndTrigCos").append(DELIMITER);
        sb.append(STARTVAL_RND).append(DELIMITER);
        rnd = new java.util.Random(STARTVAL_RND);
        for (int i = 0; i < NUMTESTS; i++) {
            sb.append(format(TrigFacade.cos(rnd.nextDouble()))).append(DELIMITER);
        }
        sb.append(DELIMITER);

        sb.append("rndTrigTan").append(DELIMITER);
        sb.append(STARTVAL_RND).append(DELIMITER);
        rnd = new java.util.Random(STARTVAL_RND);
        for (int i = 0; i < NUMTESTS; i++) {
            sb.append(format(TrigFacade.tan(rnd.nextDouble()))).append(DELIMITER);
        }
        sb.append(DELIMITER);

        sb.append("rndTrigAtan2").append(DELIMITER);
        sb.append(STARTVAL_RND).append(DELIMITER);
        rnd = new java.util.Random(STARTVAL_RND);
        for (int i = 0; i < NUMTESTS; i++) {
            sb.append(format(TrigFacade.atan2(rnd.nextDouble(), rnd.nextDouble()))).append(DELIMITER);
        }
        sb.append(DELIMITER);

        sb.append("PI").append(DELIMITER);
        sb.append(format(UPMath.PI)).append(DELIMITER);
        sb.append(format(180.0 / UPMath.PI)).append(DELIMITER);
        sb.append(DELIMITER);

        sb.append("toDegrees").append(DELIMITER);
        sb.append(STARTVAL_RND).append(DELIMITER);
        rnd = new java.util.Random(STARTVAL_RND);
        for (int i = 0; i < NUMTESTS; i++) {
            double val = rnd.nextDouble();
            sb.append(format(UPMath.toDegrees(val))).append(DELIMITER);
            sb.append(format(180.0 / UPMath.PI * val)).append(DELIMITER);
            sb.append(format(val / UPMath.PI * 180.)).append(DELIMITER);
        }
        sb.append(DELIMITER);
    }

    private static void testPow(StringBuilder sb) {
        sb.append("pow").append(DELIMITER);
        Random rnd = new java.util.Random(STARTVAL_RND);
        for (int i = 0; i < NUMTESTS; i++) {
            sb.append(format(UPMath.pow(rnd.nextDouble(), rnd.nextDouble()))).append(DELIMITER);
        }
        sb.append(DELIMITER);
    }

    private static void testTrigLookup(StringBuilder sb) {
        sb.append("trigLookupSin").append(DELIMITER);
        for (double val : FastTrigLookup.getSinArray()) {
            sb.append(format(val)).append(DELIMITER);
        }
        sb.append(DELIMITER);
        sb.append("trigLookupCos").append(DELIMITER);
        for (double val : FastTrigLookup.getCosArray()) {
            sb.append(format(val)).append(DELIMITER);
        }
        sb.append(DELIMITER);
        sb.append("trigLookupTan").append(DELIMITER);
        for (double val : FastTrigLookup.getTanArray()) {
            sb.append(format(val)).append(DELIMITER);
        }
        sb.append(DELIMITER);
        sb.append("trigLookupAsin").append(DELIMITER);
        for (double val : FastTrigLookup.getAsinArray()) {
            sb.append(format(val)).append(DELIMITER);
        }
        sb.append(DELIMITER);
        sb.append("trigLookupAcos").append(DELIMITER);
        for (double val : FastTrigLookup.getAcosArray()) {
            sb.append(format(val)).append(DELIMITER);
        }
        sb.append(DELIMITER);
    }

}
