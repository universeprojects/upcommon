package com.universeprojects.common.shared.math;

public class TrigFacade {

	public static final double sin(double rad) {
//		return Math.sin(rad);
		return FastTrigLookup.sin(rad);
	}

	public static final double cos(double rad) {
//		return Math.cos(rad);
		return FastTrigLookup.cos(rad);
	}
	
	public static final double tan(double rad) {
//		return Math.tan(rad);
		return FastTrigLookup.tan(rad);
	}

	public static final double sinDeg(double deg) {
//		return Math.sin(Math.toRadians(deg));
		return FastTrigLookup.sinDeg(deg);
	}

	public static final double cosDeg(double deg) {
//		return Math.cos(Math.toRadians(deg));
		return FastTrigLookup.cosDeg(deg);
	}
	
	public static final double tanDeg(double rad) {
//		return Math.tan(Math.toRadians(deg));
		return FastTrigLookup.tanDeg(rad);
	}
	
	public static final double asin(double val) {
//		return Math.asin(val);
		return FastTrigLookup.asin(val);
	}
	
	public static final double acos(double val) {
//		return Math.acos(val);
		return FastTrigLookup.acos(val);
	}
	
	public static final double atan(double val) {
//		return Math.atan(val);
		return FastTrigLookup.atan(val);
	}
	
	public static final double atan2(double y, double x) {
		//return Math.atan2(y, x);
		return FastTrigLookup.atan2(y, x);
	}
	
	
	
	
	
}
