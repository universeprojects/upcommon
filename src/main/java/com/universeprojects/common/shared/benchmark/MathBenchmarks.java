package com.universeprojects.common.shared.benchmark;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.universeprojects.common.shared.math.TrigFacade;
import com.universeprojects.common.shared.util.Log;
import com.universeprojects.common.shared.util.Strings;

public class MathBenchmarks {

	private Random r = new Random();

	private final static long ONE_SECOND = 1000; //ms

	private void log(String message) {
		Log.info(message);
	}

	private double randomNumber() {
		return r.nextDouble() * (r.nextInt(100) + 1);
	}

	private String convertToPercentage(double value) {
		// using ugly formatting because java.text.NumberFromat is not supported by GWT
		String str = ""+(value * 100d);
		int indexOfDecimal = str.indexOf('.');
		int trimIndex = indexOfDecimal + 3;
		if (indexOfDecimal > -1 && trimIndex <= str.length()) {
			str = str.substring(0, trimIndex);
		}
		return str+"%";
	}

	private void warmupSqrt() {
		log("Warming up...");
		long start_warmup = System.currentTimeMillis();
		long runtime_warmup = 0;
		while (runtime_warmup < 5*ONE_SECOND) {
			double num = randomNumber();
			double result1 = num * num;
			double result2 = Math.sqrt(num);

			runtime_warmup = System.currentTimeMillis() - start_warmup;
		}
		log("");
	}

	private double testSqrt() {
		long seconds = 5;
		long millis_total = seconds * ONE_SECOND;



		// SQRT
		long start_sqrt = System.currentTimeMillis();
		int count_sqrt_with_overhead = 0;
		long runtime_sqrt_total = 0;
		while (runtime_sqrt_total < millis_total) {
			double num = randomNumber();

			double result2 = Math.sqrt(num);

			runtime_sqrt_total = System.currentTimeMillis() - start_sqrt;
			count_sqrt_with_overhead++;
		}
		double runtime_sqrt_single = ((double)runtime_sqrt_total / (double)count_sqrt_with_overhead);
		log("SQRT (with overhead): " + count_sqrt_with_overhead / seconds + " per second");


		// SQR
		long start_sqr = System.currentTimeMillis();
		int count_sqr_with_overhead = 0;
		long runtime_sqr_total = 0;
		while (runtime_sqr_total < millis_total) {
			double num = randomNumber();

			double result2 = num * num;

			runtime_sqr_total = System.currentTimeMillis() - start_sqr;
			count_sqr_with_overhead++;
		}
		double runtime_sqr_single = ((double)runtime_sqr_total / (double)count_sqr_with_overhead);
		log("SQR (with overhead): " + count_sqr_with_overhead / seconds + " per second");



		double proportion = (runtime_sqrt_single / runtime_sqr_single);
		log("speed proportion SQRT/SQR: " + proportion);

		return proportion;
	}

	private void warmupTrig() {
		// WARM-UP
		log("Warming up...");
		long start_warmup = System.currentTimeMillis();
		long runtime_warmup = 0;
		while (runtime_warmup < 5*ONE_SECOND) {
			double num = randomNumber();
			double result1 = Math.sin(num);
			double result2 = TrigFacade.sin(num);

			runtime_warmup = System.currentTimeMillis() - start_warmup;
		}
	}

	private double testTrig() {
		long seconds = 5;
		long millis_total = seconds * ONE_SECOND;


		// LOOKUP TRIG
		log("Testing lookup trig runtime...");
		int count_lookup = 0;
		long start_lookup = System.currentTimeMillis();
		long runtime_lookup = 0;
		while (runtime_lookup < millis_total) {
			double num = randomNumber();

			double result = TrigFacade.sin(num);

			runtime_lookup = System.currentTimeMillis() - start_lookup;
			count_lookup++;
		}
		double runtime_lookup_single = (double)runtime_lookup / (double)count_lookup;
		log("Lookup trig (with overhead): " + count_lookup / seconds + " per second");


		// PRECISE TRIG
		log("Testing precise trig runtime...");
		int count_precise = 0;
		long start_precise = System.currentTimeMillis();
		long runtime_precise = 0;
		while (runtime_precise < millis_total) {
			double num = randomNumber();

			double result = Math.sin(num);

			runtime_precise = System.currentTimeMillis() - start_precise;
			count_precise++;
		}
		double runtime_precise_single = ((double)runtime_precise / (double)count_precise);
		log("Precise trig (with overhead): " + count_precise / seconds + " per second");


		double proportion = (runtime_precise_single / runtime_lookup_single);
		log("speed proportion Precise/Lookup: " + proportion);

		return proportion;
	}

	private double avg(List<Double> values) {
		double sum = 0;
		for (double value : values) {
			sum += value;
		}

		return sum / values.size();
	}

	private void doSqrt() {

		warmupSqrt();

		List<Double> proportions = new ArrayList<Double>();
		int cycle = 0;
		while (cycle < 10) {
			cycle++;
			log("CYCLE " + cycle);
			log("=========");
			double proportion = testSqrt();
			proportions.add(proportion);
			log("");
		}

		log("Proportions(" + proportions.size() + "): [" + Strings.join(proportions, ", ") + "]");
		double avgProp = avg(proportions);
		log("Average value = " + avgProp);

		if (avgProp > 1) {
			double gain = avgProp - 1;
			log("SQR is " + convertToPercentage(gain) + " faster than SQRT");
		}
		else {
			double loss = 1 - avgProp;
			log("SQR is " + convertToPercentage(loss) + " slower than SQRT");
		}

	}

	public void doTrig() {

		warmupTrig();

		List<Double> proportions = new ArrayList<Double>();
		int cycle = 0;
		while (cycle < 10) {
			cycle++;
			log("CYCLE " + cycle);
			log("=========");
			double proportion = testTrig();
			proportions.add(proportion);
			log("");
		}

		log("Proportions(" + proportions.size() + "): [" + Strings.join(proportions, ", ") + "]");
		double avgProp = avg(proportions);
		log("Average value = " + avgProp);

		if (avgProp > 1) {
			double gain = avgProp - 1;
			log("LOOKUP is " + convertToPercentage(gain) + " faster than PRECISE");
		}
		else {
			double loss = 1 - avgProp;
			log("LOOKUP is " + convertToPercentage(loss) + " slower than PRECISE");
		}

	}

	public static void main(String[] args) {
		new MathBenchmarks().doTrig();
	}

}
