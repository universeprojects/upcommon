package com.universeprojects.common.shared.networking;

import java.io.Serializable;

public interface NetworkConnection extends Serializable {
    void sendMessage(String data);

    void close(String message, boolean temporary);

    String getSessionId();
}
