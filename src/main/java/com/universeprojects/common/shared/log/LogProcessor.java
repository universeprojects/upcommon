package com.universeprojects.common.shared.log;

public abstract class LogProcessor {
    public Logger createLogger(String name) {
        return new Logger(name, this);
    }

    public void setRemoteLogging(boolean enabled) {
        //Stub used by RemoteLogProcessors. Necessary because of GWT access.
    }

    public Marker createMarker(String name) {
        return new Marker(name);
    }

    protected abstract void processLogEntry(long timestamp, String loggerName, LogLevel level, Marker marker, String message, Throwable throwable);

    public abstract LoggerConfigManager getLoggerConfigManager();

}
