package com.universeprojects.common.shared.log;


public class SimpleLoggerConfigManager extends LoggerConfigManager {
    private final LoggerConfig rootLoggerConfig;

    public SimpleLoggerConfigManager() {
        this.rootLoggerConfig = new LoggerConfig(LoggerFactory.ROOT_LOGGER_NAME);
        rootLoggerConfig.setMinLevel(LogLevel.INFO);
    }

    public LoggerConfig findLoggerConfig(String name) {
        return rootLoggerConfig;
    }

    @Override
    protected void setLoggerConfigInternal(String name, LogLevel level) {
        if (name.equals(LoggerFactory.ROOT_LOGGER_NAME)) {
            rootLoggerConfig.setMinLevel(level);
        }
    }

    @Override
    protected void setMarkerFilterInternal(String name, MarkerFilterType type) {
    }

    public MarkerFilter findMarkerFilter(String name) {
        return null;
    }
}
