package com.universeprojects.common.shared.log;

import java.util.Map;


@SuppressWarnings("unused")
public abstract class LoggerConfigManager {
    public abstract MarkerFilter findMarkerFilter(String name);

    public abstract LoggerConfig findLoggerConfig(String name);

    public void setLoggerConfig(String name, LogLevel level) {
        setLoggerConfigInternal(name, level);
        onLoggerConfigChange();
    }

    public void setMarkerFilter(String name, MarkerFilterType type) {
        setMarkerFilterInternal(name, type);
        onMarkerConfigChange();
    }

    protected abstract void setLoggerConfigInternal(String name, LogLevel level);

    protected abstract void setMarkerFilterInternal(String name, MarkerFilterType type);


    protected void onLoggerConfigChange() {

    }

    protected void onMarkerConfigChange() {

    }

    public void setLoggerConfig(Map<?, ?> map) {
        for (Map.Entry<?, ?> entry : map.entrySet()) {
            String name = (String) entry.getKey();
            LogLevel level;
            if (entry.getValue() instanceof LogLevel) {
                level = (LogLevel) entry.getValue();
            } else {
                level = LogLevel.valueOf(((String) entry.getValue()).toUpperCase());
            }
            setLoggerConfigInternal(name, level);
        }
        onLoggerConfigChange();
    }

    public void setMarkerFilter(Map<?, ?> map) {
        for (Map.Entry<?, ?> entry : map.entrySet()) {
            String name = (String) entry.getKey();
            MarkerFilterType type;
            if (entry.getValue() instanceof MarkerFilterType) {
                type = (MarkerFilterType) entry.getValue();
            } else {
                type = MarkerFilterType.valueOf(((String) entry.getValue()).toUpperCase());
            }
            setMarkerFilterInternal(name, type);
        }
        onMarkerConfigChange();
    }

    public void setCombinedConfig(Map<?, ?> map) {
        Object loggersObject = map.get("loggers");
        if (loggersObject instanceof Map) {
            setLoggerConfig((Map<?, ?>) loggersObject);
        }
        Object markersObject = map.get("markers");
        if (markersObject instanceof Map) {
            setMarkerFilter((Map<?, ?>) markersObject);
        }
    }
}
