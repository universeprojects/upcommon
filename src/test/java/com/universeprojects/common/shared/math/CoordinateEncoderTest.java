package com.universeprojects.common.shared.math;

import com.universeprojects.common.server.test.UnitTestBase;
import org.junit.Assert;
import org.junit.Test;

import java.util.Random;

@SuppressWarnings("MagicNumber")
public class CoordinateEncoderTest extends UnitTestBase {

    @Test
    public void simpleEncodingTest() {
        CoordinateEncoder encoder = new CoordinateEncoder(0);
        testEncoding(encoder, 0, 0);
        testEncoding(encoder, 7, 9);
        testEncoding(encoder, (1 << 30), 0);
        testEncoding(encoder, 0, (1 << 30));
        testEncoding(encoder, (1 << 30), (1 << 30));
    }

    @Test
    public void randomEncodingTest() {
        final long seed = 12352435L;
        final int range = 1 << 20;
        CoordinateEncoder encoder = new CoordinateEncoder(range);
        Random random = new Random(seed);
        for (int i = 0; i < 100; i++) {
            int x = random.nextInt(2 * range) - range;
            int y = random.nextInt(2 * range) - range;
            testEncoding(encoder, x, y);
        }
    }

    private void testEncoding(CoordinateEncoder encoder, int x, int y) {
        int[] input = {x, y};
        long encoded = encoder.encodeLocHash(x, y);
        int[] decoded = encoder.decodeLocHash(encoded);
        Assert.assertArrayEquals(input, decoded);
    }

    @Test
    public void inequalitySimpleTest() {
        CoordinateEncoder encoder = new CoordinateEncoder(100);
        long encodedValue = encoder.encodeLocHash(-20, 30);
        Assert.assertTrue(encoder.encodeLocHash(-21, 30) < encodedValue);
        Assert.assertTrue(encoder.encodeLocHash(-20, 29) < encodedValue);
        Assert.assertTrue(encoder.encodeLocHash(-19, 30) > encodedValue);
        Assert.assertTrue(encoder.encodeLocHash(-20, 31) > encodedValue);
    }

    @Test
    public void inequalityAreaTest() {
        testArea(new CoordinateEncoder(0), 32, 376547);
        testArea(new CoordinateEncoder(1 << 29), (1 << 29) - 1, -(1 << 29) + 1);
    }

    private void testArea(CoordinateEncoder encoder, int centerX, int centerY) {
        long center = encoder.encodeLocHash(centerX, centerY);
        long top = encoder.encodeLocHash(centerX, centerY - 1);
        long topLeft = encoder.encodeLocHash(centerX - 1, centerY - 1);
        long topRight = encoder.encodeLocHash(centerX + 1, centerY - 1);
        long left = encoder.encodeLocHash(centerX - 1, centerY);
        long right = encoder.encodeLocHash(centerX + 1, centerY);
        long bottom = encoder.encodeLocHash(centerX, centerY + 1);
        long bottomLeft = encoder.encodeLocHash(centerX - 1, centerY + 1);
        long bottomRight = encoder.encodeLocHash(centerX + 1, centerY + 1);

        Assert.assertTrue(topLeft < top);
        Assert.assertTrue(top < topRight);

        Assert.assertTrue(left < center);
        Assert.assertTrue(center < right);

        Assert.assertTrue(bottomLeft < bottom);
        Assert.assertTrue(bottom < bottomRight);

        Assert.assertTrue(topLeft < left);
        Assert.assertTrue(left < bottomLeft);

        Assert.assertTrue(top < center);
        Assert.assertTrue(center < bottom);

        Assert.assertTrue(topRight < right);
        Assert.assertTrue(right < bottomRight);
    }


    @Test
    public void testBoundaries() {
        CoordinateEncoder encoder = new CoordinateEncoder(0);
        try {
            //noinspection NumericOverflow
            encoder.encodeLocHash(1 << 31, 0);
            Assert.fail();
        } catch (IllegalArgumentException ignored) {
        }
    }
}
