package com.universeprojects.common.shared.math;

import java.util.Arrays;

import org.junit.Ignore;
import org.junit.Test;

import com.universeprojects.common.server.test.UnitTestBase;


public class TrigLookupTest extends UnitTestBase {

	private static final int PRECISION_CARDINAL_DEG = -10;
	private static final int PRECISION_CARDINAL_RAD = -10;
	private static final int PRECISION_CARDINAL_ARC = -10;
	private static final int PRECISION_FULL_RANGE_DEG = -2;
	private static final int PRECISION_FULL_RANGE_RAD = -2;
	private static final int PRECISION_FULL_RANGE_ARC = -1;
	
	
	private void check(double result1, double result2, int precision, double... inputs) {
		double threshold = 1;
		if (precision > 0) {
			threshold = Math.pow(10, precision);
		} else if(precision < 0) {
			threshold = 1 / Math.pow(10, -precision);
		}
		
		double d = Math.abs(result1 - result2);
		if (d > threshold) {
			throw new RuntimeException("Too big of a difference (" + d + ") between " + 
					"result1: " + result1 + " and result2: " + result2 + " for "+Arrays.toString(inputs)+" with threshold "+threshold);
		}
	}
	
	@Test
	public void testCheckingMechanism() {
		
		double d1 = 0.123456789;
		double d2 = 0.123499999;
		
		check(d1, d2, -1, 0);
		check(d1, d2, -2, 0);
		check(d1, d2, -3, 0);
		check(d1, d2, -4, 0);
		
		try {
			check(d1, d2, -5, 0);
			failIfNoExceptionThrown();
		}
		catch (RuntimeException e) {
			assertExceptionMessageContains(e, "Too big of a difference");
		}
		
		try {
			check(d1, d2, -6, 0);
			failIfNoExceptionThrown();
		}
		catch (RuntimeException e) {
			assertExceptionMessageContains(e, "Too big of a difference");
		}
		
		try {
			check(d1, d2, -7, 0);
			failIfNoExceptionThrown();
		}
		catch (RuntimeException e) {
			assertExceptionMessageContains(e, "Too big of a difference");
		}
	}
	
	@Test
	public void cardinalDirections_deg() {
		for (double deg = 0; deg <= 270; deg += 90) {
			double rad = Math.toRadians(deg);
			check(FastTrigLookup.sinDeg(deg), Math.sin(rad), PRECISION_CARDINAL_DEG, deg);
			check(FastTrigLookup.cosDeg(deg), Math.cos(rad), PRECISION_CARDINAL_DEG, deg);
			check(FastTrigLookup.tanDeg(deg), Math.tan(rad), PRECISION_CARDINAL_DEG, deg);
		}
	}
	
	@Test
	public void cardinalDirections_rad() {
		for (double deg = 0; deg <= 270; deg += 90) {
			double rad = Math.toRadians(deg);
			check(FastTrigLookup.sin(rad), Math.sin(rad), PRECISION_CARDINAL_RAD, deg);
			check(FastTrigLookup.cos(rad), Math.cos(rad), PRECISION_CARDINAL_RAD, deg);
			check(FastTrigLookup.tan(rad), Math.tan(rad), PRECISION_CARDINAL_RAD, deg);
		}
	}
	
	@Test
	public void cardinalDirections_deg_overflow() {
		for (int deg = -720; deg <= 720; deg += 90) {
			double rad = Math.toRadians(deg);
			check(FastTrigLookup.sinDeg(deg), Math.sin(rad), PRECISION_CARDINAL_DEG, deg);
			check(FastTrigLookup.cosDeg(deg), Math.cos(rad), PRECISION_CARDINAL_DEG, deg);
			//check(FastTrigLookupDouble.tanDeg(deg), Math.tan(rad), PRECISION_CARDINAL_DEG, deg);
		}
	}
	
	@Test
	public void cardinalDirections_rad_overflow() {
		for (int deg = -720; deg <= 720; deg += 90) {
			double rad = Math.toRadians(deg);
			check(FastTrigLookup.sin(rad), Math.sin(rad), PRECISION_CARDINAL_RAD, deg);
			check(FastTrigLookup.cos(rad), Math.cos(rad), PRECISION_CARDINAL_RAD, deg);
			//check(FastTrigLookupDouble.tan(rad), Math.tan(rad), PRECISION_CARDINAL_RAD, deg);
		}
	}
	
	@Test
	public void fullRange_deg() {
		for (double deg = 0; deg < 360; deg += 0.01) {
			double rad = Math.toRadians(deg);
			check(FastTrigLookup.sinDeg(deg), Math.sin(rad), PRECISION_FULL_RANGE_DEG, deg);
			check(FastTrigLookup.cosDeg(deg), Math.cos(rad), PRECISION_FULL_RANGE_DEG, deg);
			//check(FastTrigLookupDouble.tanDeg(deg), Math.tan(rad), PRECISION_FULL_RANGE_RAD, deg);
		}
	}
	
	@Test
	public void fullRange_rad() {
		for (double deg = 0; deg < 360; deg += 0.01) {
			double rad = Math.toRadians(deg);
			check(FastTrigLookup.sin(rad), Math.sin(rad), PRECISION_FULL_RANGE_RAD, deg);
			check(FastTrigLookup.cos(rad), Math.cos(rad), PRECISION_FULL_RANGE_RAD, deg);
			//check(FastTrigLookupDouble.tan(rad), Math.tan(rad), PRECISION_FULL_RANGE_RAD, deg);
		}
	}
	
	@Test
	public void fullRange_arcus() {
		for (double val = 0; val < 360; val += 0.01) {
			check(FastTrigLookup.asin(val), Math.asin(val), PRECISION_FULL_RANGE_ARC, val);
			check(FastTrigLookup.acos(val), Math.acos(val), PRECISION_FULL_RANGE_ARC, val);
			check(FastTrigLookup.atan(val), Math.atan(val), PRECISION_FULL_RANGE_ARC, val);
		}
	}
	
	@Test
	public void fullRange_deg_overflow() {
		for (double deg = -720; deg <= 720; deg += 0.01) {
			double rad = Math.toRadians(deg);
			check(FastTrigLookup.sinDeg(deg), Math.sin(rad), PRECISION_FULL_RANGE_DEG, deg);
			check(FastTrigLookup.cosDeg(deg), Math.cos(rad), PRECISION_FULL_RANGE_DEG, deg);
			//check(FastTrigLookupDouble.tanDeg(deg), Math.tan(rad), PRECISION_FULL_RANGE_DEG, deg);
		}
	}
	
	@Test
	public void fullRange_rad_overflow() {
		for (double deg = -720; deg <= 720; deg += 0.01) {
			double rad = Math.toRadians(deg);
			check(FastTrigLookup.sin(rad), Math.sin(rad), PRECISION_FULL_RANGE_RAD, deg);
			check(FastTrigLookup.cos(rad), Math.cos(rad), PRECISION_FULL_RANGE_RAD, deg);
			//check(FastTrigLookupDouble.tan(rad), Math.tan(rad), PRECISION_FULL_RANGE_RAD, deg);
		}
	}

	@Ignore
	@Test
	public void atan2() {
		check(FastTrigLookup.atan2(0, -100), Math.atan2(0, -100), PRECISION_FULL_RANGE_ARC, 0,-100);
		for(double x = -100;x<=100;x+=0.1) {
			for(double y = -100;y<=100;y+=0.1) {
				check(FastTrigLookup.atan2(y, x), Math.atan2(y, x), PRECISION_FULL_RANGE_ARC, y,x);
			}
		}
	}
	
}
