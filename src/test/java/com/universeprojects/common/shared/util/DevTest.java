package com.universeprojects.common.shared.util;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.junit.Test;

import com.universeprojects.common.server.test.UnitTestBase;

public class DevTest extends UnitTestBase {

	@Test
	public void checkNotNull_pass() {
		Dev.checkNotNull(new Object());
		Dev.checkNotNull(new BigDecimal(5));
		Dev.checkNotNull(new ArrayList<Double>());
		Dev.checkNotNull(new StringBuffer());
		Dev.checkNotNull("");
		Dev.checkNotNull(" ");
		Dev.checkNotNull("     ");
		Dev.checkNotNull("a");
		Dev.checkNotNull("abc");
	}
	
	@Test
	public void checkNotNull_fail() {
		try {
			Dev.checkNotNull(null);
			failIfNoExceptionThrown();
		}
		catch (Exception ex) {
			assertExceptionType(ex, DevException.class);
			assertExceptionMessageContains(ex, "Argument is null");
		}
	}
	
	@Test
	public void checkNotEmpty_pass() {
		Dev.checkNotEmpty("a");
		Dev.checkNotEmpty("abc");
	}
	
	@Test
	public void checkNotEmpty_fail_Null() {
		try {
			Dev.checkNotEmpty(null);
			failIfNoExceptionThrown();
		}
		catch (Exception ex) {
			assertExceptionType(ex, DevException.class);
			assertExceptionMessageContains(ex, "String argument is null or empty");
		}
	}
	
	@Test
	public void checkNotEmpty_fail_EmptyString() {
		try {
			Dev.checkNotEmpty("");
			failIfNoExceptionThrown();
		}
		catch (Exception ex) {
			assertExceptionType(ex, DevException.class);
			assertExceptionMessageContains(ex, "String argument is null or empty");
		}
	}
	
	@Test
	public void checkNotEmpty_fail_BlankStringSingleSpace() {
		try {
			Dev.checkNotEmpty(" ");
			failIfNoExceptionThrown();
		}
		catch (Exception ex) {
			assertExceptionType(ex, DevException.class);
			assertExceptionMessageContains(ex, "String argument is null or empty");
		}
	}
	
	@Test
	public void checkNotEmpty_fail_BlankStringMultipleSpaces() {
		try {
			Dev.checkNotEmpty("      ");
			failIfNoExceptionThrown();
		}
		catch (Exception ex) {
			assertExceptionType(ex, DevException.class);
			assertExceptionMessageContains(ex, "String argument is null or empty");
		}
	}
	
}
